
import java.util.Stack;

public class QueueFromStacks {
private Stack<Integer> A; //stacks of integers 
private Stack<Integer> B;


public QueueFromStacks(){ //constructor
	A = new Stack<Integer>(); //empty stacks
	B = new Stack<Integer>();
	
	
}
public void enqueue(int item){
		A.push(item); //pushing item onto A, worst case runtime O(1)
		
}
public int dequeue(){
	if (B.isEmpty()){ //worst runtime- O(n) when using the for loop
		int y = A.size(); //for range
		for (int i=0; i<y;i++){ //pop until A is empty
		B.push(A.pop()); //push onto B all popped items of A 
		}
	int x = B.pop(); //pop last item
	return x; //popped integer 
	}
	else{
		int x = B.pop(); //pop last item if stack isn't empty
	return x; //popped integer
	}
}
public boolean isEmpty(){
	if (A.isEmpty() && B.isEmpty()){ //checks if they are empty
	return true; //returns true if empty 
	}
	else{
		return false; //returns false if not
		} //runtime O(1)
	}
public int size() { //find size of queue, for testing
    return A.size() + B.size();   //O(1)  
}

}	


